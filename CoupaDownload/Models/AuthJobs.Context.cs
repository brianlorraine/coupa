﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoupaDownload.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ClarkDBEntities : DbContext
    {
        public ClarkDBEntities()
            : base("name=ClarkDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual ObjectResult<RetrieveJobs_Result> RetrieveJobs(Nullable<byte> userLevel, string mCMCU)
        {
            var userLevelParameter = userLevel.HasValue ?
                new ObjectParameter("UserLevel", userLevel) :
                new ObjectParameter("UserLevel", typeof(byte));
    
            var mCMCUParameter = mCMCU != null ?
                new ObjectParameter("MCMCU", mCMCU) :
                new ObjectParameter("MCMCU", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<RetrieveJobs_Result>("RetrieveJobs", userLevelParameter, mCMCUParameter);
        }
    }
}
