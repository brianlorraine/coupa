﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using CoupaDownload.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Security;

namespace CoupaDownload.Controllers
{

    public class HomeController : KPIController
    {
       
        //private App_KPI_2015EntitiesJobs jdejobs = new App_KPI_2015EntitiesJobs();
        private ClarkDBEntities dbjobs = new ClarkDBEntities();

        static string strDelimitor = "\\";
        static string[] words = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new[] { strDelimitor }, StringSplitOptions.None);

        public ActionResult Download()
        {
            return View();
        }

        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            if (CSInclude.SSO_Check_LocalIP(Request.ServerVariables["REMOTE_ADDR"].ToString()) != 0)
            {
                if (Request.QueryString["ReturnUrl"] != null || Request.QueryString["ReturnID"] != null)
                {
                    if (Request.QueryString["ReturnID"] == null)
                    {
                        string SingleSignOnURL = System.Configuration.ConfigurationManager.AppSettings["KPISingleSignOnURL"].ToString();
                        string ReturnID = CSInclude.SSO_Create_Ticket(Request.ServerVariables["REMOTE_ADDR"].ToString(), CSInclude.Build_ApplicationPath(true)).ToString();
                        Response.Redirect(SingleSignOnURL + "?ReturnID=" + ReturnID + "&ReturnPath=" + CSInclude.Build_ApplicationPath(false));
                        Response.End();
                    }
                    else
                    {
                        string RtnKey = CSInclude.SSO_Delete_Ticket(Request.QueryString["ReturnID"]).ToString();
                        if (RtnKey != "")
                        {

                            FormsAuthentication.RedirectFromLoginPage(RtnKey, false);

                            Response.End();
                        }
                    }
                }
            }

            // If server variables are missing or there is no "ReturnURL" or "ReturnID" in the querystring, then something is wrong.
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }


        [HttpGet]
        public ActionResult Jobs()
        {
            return View(dbjobs.RetrieveJobs(Convert.ToByte(TempData.Peek("UserAccessLevel").ToString()), null));
            //return View(jdejobs.Bases.ToList());

        }

        [HttpPost]
        public ActionResult Jobs(string searchString)
        {
            //  var q = jdejobs.Bases.ToList();
            var q = dbjobs.RetrieveJobs(Convert.ToByte(TempData.Peek("UserAccessLevel").ToString()), null).ToList();
            if (searchString != null)
            {
                if (searchString.Trim() != "")
                {
                    q = q.Where(s => s.MCDL01.ToLower().Contains(searchString.Trim().ToLower()) || s.MCDL01.ToLower().Contains(searchString.Trim().ToLower())).ToList();
                }
            }

            return View(q);
        }

        // GET: 
        [HttpGet]
        [Authorize]
        public ActionResult Default()
        {

            /***********************************************************************************************/
            /* This code should only execute the first time the page is loaded, hence why it is in the GET */
            /***********************************************************************************************/

            string ApplicationVirtualPath = "~" + Request.CurrentExecutionFilePath + ((Request.ServerVariables["Query_String"] == "") ? "" : ("?" + Request.ServerVariables["Query_String"]));
            string ConnString = "KPI_Security";
            string SqlCmd = "KPISecurity2018.Application_CheckAccess";
            SqlConnection oSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[ConnString].ConnectionString);
            SqlCommand oSqlCommand = new SqlCommand(SqlCmd, oSqlConnection);

            try
            {
                oSqlConnection.Open();
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(oSqlCommand);
                oSqlCommand.Parameters["@AUID"].Value = Session["KPISystemUserID"];
                //Response.Write("KPISystemUserID: " + Session["KPISystemUserID"].ToString() + "<br/>");
                oSqlCommand.Parameters["@NodeAddress"].Value = ApplicationVirtualPath;
                //Response.Write("Virtual Path: " + ApplicationVirtualPath + "<br/>");
                oSqlCommand.ExecuteNonQuery();

                TempData["UserAccessLevel"] = oSqlCommand.Parameters["@Level"].Value;
                TempData["UserAdminURL"] = oSqlCommand.Parameters["@AdminURL"].Value;
                TempData["HelpFileURL"] = oSqlCommand.Parameters["@HelpURL"].Value;
                TempData["KeyType"] = oSqlCommand.Parameters["@KEID"].Value;
                //Response.Write("CheckAccessKLeyType: " + TempData.Peek("KeyType").ToString() + "<br/>");
                TempData["CompanyLogo"] = oSqlCommand.Parameters["@CompanyLogo"].Value.ToString().Replace("~", "");
                TempData["ApplicationName"] = oSqlCommand.Parameters["@ApplicationName"].Value;

                oSqlCommand.Dispose();
                oSqlConnection.Close();
                Session["OBLoggedIn"] = true;

                switch (Convert.ToInt16(TempData.Peek("KeyType")))
                {
                    case 1010:
                        TempData["LevelMin"] = 50;
                        break;
                    case 1020:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1030:
                        TempData["LevelMin"] = 10;
                        break;
                    case 1060:
                        TempData["LevelMin"] = 10;
                        break;
                    case 6010:
                        TempData["LevelMin"] = 10;
                        break;
                    default:
                        TempData["LevelMin"] = 10;
                        break;
                }

            }
            catch (Exception ex)
            {
                oSqlCommand.Dispose();
                oSqlConnection.Close();
                ViewBag.Error = ex.Message;
                return View();
            }

            /*******************************************************************/
            /*******************************************************************/
            /*******************************************************************/


            ViewBag.Message = null;
            ViewBag.Message2 = null;


            string dbstring = "KPI_IPM";
            string dbSqlCmd = "IPMWeb2017.CoupaDownload";
            SqlConnection dbSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[dbstring].ConnectionString);
            SqlCommand dbSqlCommand = new SqlCommand(dbSqlCmd, dbSqlConnection);
            SqlDataAdapter dbSqlDataAdapter = new SqlDataAdapter();
            DataSet dbDataSet = new DataSet();
            dbSqlConnection.Open();
            dbSqlCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param = new SqlParameter("@CDAUID", new Guid(Session["KPISystemUserID"].ToString()));
            dbSqlCommand.Parameters.Add(param);
            SqlParameter param2 = new SqlParameter("@Option", 50);
            dbSqlCommand.Parameters.Add(param2);
            
            dbSqlDataAdapter.SelectCommand = dbSqlCommand;
            dbSqlDataAdapter.Fill(dbDataSet, "Last_List");

            var model = new Invoice { };

            if (dbDataSet.Tables[0].Rows.Count > 0)
            {
                DataRow cdrow = dbDataSet.Tables[0].Rows[0];
                if (cdrow["CDCOstCode"] != DBNull.Value) { 
                    model.costcode = cdrow["CDCOstCode"].ToString();
                }
                if (cdrow["CDJobNo"] != DBNull.Value)
                {
                    model.jobnumber = Convert.ToInt32(cdrow["CDJobNo"].ToString());
                }
                if (cdrow["CDVendorNo"] != DBNull.Value)
                {
                    model.vendor = Convert.ToInt32(cdrow["CDVendorNo"].ToString());
                }
                if (cdrow["CDInvoiceNo"] != DBNull.Value)
                {
                    model.invoicenumber = cdrow["CDInvoiceNo"].ToString();
                }
                if (cdrow["CDInvoiceDateBeg"] != DBNull.Value)
                {
                    model.invoicedatestart = Convert.ToDateTime(cdrow["CDInvoiceDateBeg"].ToString());
                }
                if (cdrow["CDInvoiceDateEnd"] != DBNull.Value)
                {
                    model.invoicedateend = Convert.ToDateTime(cdrow["CDInvoiceDateEnd"].ToString());
                }
                if (cdrow["CDGLDateBeg"] != DBNull.Value)
                {
                    model.gldatestart = Convert.ToDateTime(cdrow["CDGLDateBeg"].ToString());
                }
                if (cdrow["CDGLDateEnd"] != DBNull.Value)
                {
                    model.gldateend = Convert.ToDateTime(cdrow["CDGLDateEnd"].ToString());
                }


            }

            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Default(DateTime? gldatestart, DateTime? gldateend, int? jobnumber, string invoicenumber, int? vendor, DateTime? invoicedatestart, DateTime? invoicedateend, string costcode, string filelocation, string filepath)
        {

            if (filelocation != null)
            {
                ViewBag.Message = null;
                ViewBag.Message2 = null;
                ViewBag.Message3 = null;
                System.IO.File.Delete(filelocation);
                System.IO.Directory.Delete(filepath, true);
                string dbstring = "KPI_IPM";
                string dbSqlCmd = "IPMWeb2017.CoupaDownload";
                SqlConnection dbSqlConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings[dbstring].ConnectionString);
                SqlCommand dbSqlCommand = new SqlCommand(dbSqlCmd, dbSqlConnection);
                SqlDataAdapter dbSqlDataAdapter = new SqlDataAdapter();
                DataSet dbDataSet = new DataSet();
                dbSqlConnection.Open();
                dbSqlCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@CDAUID", new Guid(Session["KPISystemUserID"].ToString()));
                dbSqlCommand.Parameters.Add(param);
                SqlParameter param2 = new SqlParameter("@Option", 50);
                dbSqlCommand.Parameters.Add(param2);

                dbSqlDataAdapter.SelectCommand = dbSqlCommand;
                dbSqlDataAdapter.Fill(dbDataSet, "Last_List");

                var model = new Invoice { };

                if (dbDataSet.Tables[0].Rows.Count > 0)
                {
                    DataRow cdrow = dbDataSet.Tables[0].Rows[0];
                    if (cdrow["CDCOstCode"] != DBNull.Value)
                    {
                        model.costcode = cdrow["CDCOstCode"].ToString();
                    }
                    if (cdrow["CDJobNo"] != DBNull.Value)
                    {
                        model.jobnumber = Convert.ToInt32(cdrow["CDJobNo"].ToString());
                    }
                    if (cdrow["CDVendorNo"] != DBNull.Value)
                    {
                        model.vendor = Convert.ToInt32(cdrow["CDVendorNo"].ToString());
                    }
                    if (cdrow["CDInvoiceNo"] != DBNull.Value)
                    {
                        model.invoicenumber = cdrow["CDInvoiceNo"].ToString();
                    }
                    if (cdrow["CDInvoiceDateBeg"] != DBNull.Value)
                    {
                        model.invoicedatestart = Convert.ToDateTime(cdrow["CDInvoiceDateBeg"].ToString());
                    }
                    if (cdrow["CDInvoiceDateEnd"] != DBNull.Value)
                    {
                        model.invoicedateend = Convert.ToDateTime(cdrow["CDInvoiceDateEnd"].ToString());
                    }
                    if (cdrow["CDGLDateBeg"] != DBNull.Value)
                    {
                        model.gldatestart = Convert.ToDateTime(cdrow["CDGLDateBeg"].ToString());
                    }
                    if (cdrow["CDGLDateEnd"] != DBNull.Value)
                    {
                        model.gldateend = Convert.ToDateTime(cdrow["CDGLDateEnd"].ToString());
                    }


                }

                return View(model);
            }
            else
            {

                if (ModelState.IsValid)
                {

                     
                     
                      

                    using (var con = new SqlConnection

                     (ConfigurationManager.ConnectionStrings["KPI_IPM"].ConnectionString))
                      
                    {

                        var cmd = new SqlCommand("IPMWeb2017.CoupaDownload", con);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar)).Value = Session["KPISystemUserName"].ToString();
                        cmd.Parameters.Add(new SqlParameter("@Option", SqlDbType.SmallInt)).Value = 70;
                        cmd.Parameters.Add(new SqlParameter("@CDAUID", SqlDbType.UniqueIdentifier)).Value = new Guid(Session["KPISystemUserID"].ToString());
                        if (jobnumber != null) {
                            cmd.Parameters.Add(new SqlParameter("@CDJobNo", SqlDbType.VarChar)).Value = jobnumber;
                        }
                        if (vendor != null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@CDVendorNo", SqlDbType.VarChar)).Value = vendor;
                        }
                        if (invoicenumber != null)
                        {
                            if (invoicenumber != "")
                            {
                                cmd.Parameters.Add(new SqlParameter("@CDInvoiceNo", SqlDbType.VarChar)).Value = invoicenumber;
                            }
                            
                        }
                        if (costcode != null)
                        {
                            if (invoicenumber != "")
                            {
                                cmd.Parameters.Add(new SqlParameter("@CDCostCode", SqlDbType.VarChar)).Value = costcode;
                            }

                        }
                        
                        cmd.Parameters.Add(new SqlParameter("@CDInvoiceDateBeg", SqlDbType.Date)).Value = invoicedatestart;
                        cmd.Parameters.Add(new SqlParameter("@CDInvoiceDateEnd", SqlDbType.Date)).Value = invoicedateend;
                        cmd.Parameters.Add(new SqlParameter("@CDGLDateBeg", SqlDbType.Date)).Value = gldatestart;
                        cmd.Parameters.Add(new SqlParameter("@CDGLDateEnd", SqlDbType.Date)).Value = gldateend;



                        if (con.State != ConnectionState.Open)
                            con.Open();

                        cmd.ExecuteNonQuery();

                        if (con.State != ConnectionState.Closed)
                            con.Close();

                    }

                    ViewBag.Message = null;
                    ViewBag.Message2 = null;
                    var parameters1 = HttpUtility.ParseQueryString(string.Empty);
                    var parameters2 = HttpUtility.ParseQueryString(string.Empty);

                    Guid id = Guid.NewGuid();

                    string currentyear = DateTime.Now.Year.ToString();
                    string currentmonth = DateTime.Now.Month.ToString();
                    if (currentmonth.Length == 1)
                    {
                        currentmonth = "0" + currentmonth;
                    }
                    string currentday = DateTime.Now.Day.ToString();
                    if (currentday.Length == 1)
                    {
                        currentday = "0" + currentday;
                    }
                    string datepath = currentyear + "/" + currentmonth + "/" + currentday + "/";

                    parameters1["invoice-lines[account-allocations][account][segment-1]"] = jobnumber.ToString();
                    parameters2["invoice-lines[account][segment-1]"] = jobnumber.ToString();
                    parameters1["subfolder"] = datepath + "KPI_" + id + "/" + jobnumber.ToString() + "/partial/";
                    parameters2["subfolder"] = datepath + "KPI_" + id + "/" + jobnumber.ToString() + "/full/";



                    if (costcode != "")
                    {
                        parameters1["invoice-lines[account-allocations][account][segment-2]"] = costcode;
                        parameters2["invoice-lines[account][segment-2]"] = costcode;
                    }

                    if (gldatestart != null)
                    {
                        DateTime mydate = Convert.ToDateTime(gldatestart);
                        parameters1["gl-date[gt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                        parameters2["gl-date[gt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                    }
                    if (gldateend != null)
                    {
                        DateTime mydate = Convert.ToDateTime(gldateend);
                        parameters1["gl-date[lt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                        parameters2["gl-date[lt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                    }
                    if (invoicenumber != "")
                    {
                        parameters1["invoice-number"] = invoicenumber;
                        parameters2["invoice-number"] = invoicenumber;
                    }
                    if (vendor != null)
                    {
                        parameters1["supplier[number][in]"] = vendor.ToString();
                        parameters2["supplier[number][in]"] = vendor.ToString();
                    }

                    if (invoicedatestart != null)
                    {
                        DateTime mydate = Convert.ToDateTime(invoicedatestart);
                        parameters1["invoice-date[gt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                        parameters2["invoice-date[gt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                    }
                    if (invoicedateend != null)
                    {
                        DateTime mydate = Convert.ToDateTime(invoicedateend);
                        parameters1["invoice-date[lt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                        parameters2["invoice-date[lt_or_eq]"] = mydate.ToString("yyyy-MM-dd");
                    }

                    string queryString1 = System.Web.HttpUtility.UrlDecode(parameters1.ToString());
                    string queryString2 = System.Web.HttpUtility.UrlDecode(parameters2.ToString());

                    string path1 = "\\\\ccgawsftp003\\coupa$\\ImageDownloads\\Production\\" + currentyear + "\\" + currentmonth + "\\" + currentday + "\\KPI_" + id.ToString() + "\\" + jobnumber.ToString() + "\\partial";
                    string path2 = "\\\\ccgawsftp003\\coupa$\\ImageDownloads\\Production\\" + currentyear + "\\" + currentmonth + "\\" + currentday + "\\KPI_" + id.ToString() + "\\" + jobnumber.ToString() + "\\full";
                    if (!Directory.Exists(path1))
                    {
                        Directory.CreateDirectory(path1);
                    }
                    if (!Directory.Exists(path2))
                    {
                        Directory.CreateDirectory(path2);
                    }

                    var client = new HttpClient();
                    client.Timeout = TimeSpan.FromMilliseconds(300000);
                    int hitcount = 0;
                    var response2 = await client.GetAsync("http://coupaintegrationprod.cloudhub.io/downloadinvoiceattachments?" + queryString2);
                    if (response2.IsSuccessStatusCode)
                    {
                        var contents = await response2.Content.ReadAsStringAsync();
                        if (contents != "")
                        {
                            hitcount++;
                        }

                    }
                    else
                    {

                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }

                    var response = await client.GetAsync("http://coupaintegrationprod.cloudhub.io/downloadinvoiceattachments?" + queryString1);

                    if (response.IsSuccessStatusCode)
                    {
                        var contents = await response.Content.ReadAsStringAsync();
                        if (contents != "")
                        {

                            hitcount++;
                        }

                    }
                    else
                    {


                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }

                    if (hitcount > 0)
                    {
                        string pathToFiles = "\\\\ccgawsftp003\\coupa$\\ImageDownloads\\Production\\" + currentyear + "\\" + currentmonth + "\\" + currentday + "\\KPI_" + id.ToString() + "/" + jobnumber.ToString();
                        string rootpath = "\\\\ccgawsftp003\\coupa$\\ImageDownloads\\Production\\" + currentyear + "\\" + currentmonth + "\\" + currentday + "\\KPI_" + id.ToString();
                        string zippath = "\\\\ccgawsftp003\\coupa$\\ImageDownloads\\Production\\" + currentyear + "\\" + currentmonth + "\\" + currentday + "\\KPI_" + id.ToString() + "/KPI_" + id.ToString() + ".zip";


                        ZipFile.CreateFromDirectory(pathToFiles, zippath);
                        ViewBag.Message = "Invoices downloaded.";
                        ViewBag.Message2 = "downloads/" + datepath + "KPI_" + id.ToString() + "/KPI_" + id.ToString() + ".zip";
                        ViewBag.Message3 = zippath;
                        ViewBag.rootpath = rootpath;

                    }
                    else
                    {
                        ViewBag.Message = "No invoices found for job number " + jobnumber.ToString();
                    }


                }
                ModelState.Clear();
                return View();
            }

        }
    }
}